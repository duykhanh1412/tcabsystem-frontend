angular.module("reportTeamTasks").component("reportTeamTasks", {
    templateUrl: "tcabs/teamTaskReport.html",
    controller: ["Report", "Units", "Projects", "Teams", "Session", function (Report, Units, Projects, Teams, Session) {
        var self = this;
        self.isServerFailed = false;
        self.isResponseEmpty = false;
        self.isProjectNotChoosed = false;
        self.isTeamNotChoosed = false;
        self.isTeamNameExisting = false;
        self.isTeamNameNotExisting = false;
        self.isSearchSuccessfull = false;
        self.unit = {};
        self.units = [];
        self.project = {};
        self.projects = [];
        self.teamName = "";
        self.team = [];
        self.teams = [];
        self.teamTasks = [];
        self.weeks = [];
        self.week = [];
        self.totalTimeTakenUpToDate = 0;
        self.totalCostUpToDate = 0;
        self.totalTimeTakenPerWeek = 0;
        self.costPerWeek = 0;
        self.chartColors = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(231,233,237)'];

        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });
        self.contributionChartData = [];

        self.getProjects = function () {
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.projects = Projects.get({
                unitId: self.unit.id
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };

        self.searchTeams = function () {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.isTeamNameExisting = false;
            self.isTeamNameNotExisting = false;
            if (self.project == null) {
                self.isProjectNotChoosed = true;
                return;
            }
            self.teams = Teams.get({
                teamName: self.teamName,
                projectId: self.project.id
            }, function (response) {
                self.isServerFailed = false;
                self.isTeamNameNotExisting = false;
                self.isTeamNameExisting = true;
            }, function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 400) {
                    self.isProjectNotChoosed = true;
                }
            });
        };

        self.selectTeam = function () {
            self.isTeamNameExisting = false;
            self.teamName = self.team[0].name;
        };

        self.search = function () {
            self.totalCostUpToDate = 0;
            self.totalTimeTakenUpToDate = 0;
            self.contributionRates = [];
            self.labels = [];
            self.isServerFailed = false;
            self.isResponseEmpty = false;
            self.isSearchSuccessfull = false;
            self.weeks = Report.query({
                reportType: 'teamTask',
                teamId: self.team[0].id
            }, function (response) {
                self.isSearchSuccessfull = true;
                self.index1 = 0;
                angular.forEach(self.weeks[0].studentWeeklyTask, function (student) {
                    self.contributionRates[self.index1] = 0;
                    self.index1 = self.index1 + 1;
                });
                angular.forEach(self.weeks, function (week) {
                    self.totalCostUpToDate += week.totalCostPerWeekForTeam;
                    self.totalTimeTakenUpToDate += week.toltalHourPerWeekForTeam / 60;
                    self.index = 0;
                    angular.forEach(week.studentWeeklyTask, function (student) {
                        self.contributionRates[self.index] += student.totalHourPerWeek / 60;
                        self.labels[self.index] = student.studentName;
                        self.index = self.index + 1;
                    });
                });
                self.index2 = 0;
                angular.forEach(self.weeks[0].studentWeeklyTask, function (student) {
                    self.contributionRates[self.index2] = self.contributionRates[self.index2] / self.totalTimeTakenUpToDate * 100;
                    self.contributionRates[self.index2] = self.contributionRates[self.index2].toFixed(2);
                    self.index2 = self.index2 + 1;
                });
            }, function (error) {
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
            });
        };
    }]
});
