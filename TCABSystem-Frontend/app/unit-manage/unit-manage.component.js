angular.module("manageUnit").component("manageUnit", {
    templateUrl: "tcabs/unitManage.html",
    controller: ["Units", "Unit", "$location", "UnitSession", function (Units, Unit, $location, UnitSession) {
        var self = this;
        self.isServerFailed = false;
        self.isResponseEmpty = false;
        self.hasUnitAlreadyStarted = false;
        self.deleteSucessful = false;
        self.units = [];
        self.unitName = "";
        self.unitCode = "";

        self.search = function () {
            self.isServerFailed = false;
            self.isResponseEmpty = false;
            self.hasUnitAlreadyStarted = false;
            self.deleteSucessful = false;
            self.units = Units.query({
                name: self.unitName,
                code: self.unitCode
            }, function (response) {
                
            }, function (error) {
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
            });
        };

        self.delete = function (index) {
            if (confirm("Are you sure you want to delete this unit?")) {
                self.isServerFailed = false;
                self.deleteSucessful = false;
                self.hasUnitAlreadyStarted = false;

                Unit.delete({
                    unitId: self.units[index].id
                }, function (response) {
                    self.units.splice(index, 1);
                    self.hasUnitAlreadyStarted = false;
                    self.deleteSucessful = true;
                }, function (error) {
                    if (error.status === 406) {
                        self.hasUnitAlreadyStarted = true;
                    }
                    if (error.status === 500) {
                        self.isServerFailed = true;
                    }
                });
            }
        };

        self.update = function (index) {
            UnitSession.create(self.units[index]);
            $location.path("/updateUnit").replace();
        };
    }]
});
