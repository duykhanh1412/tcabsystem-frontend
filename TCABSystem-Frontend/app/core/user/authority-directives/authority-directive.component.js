angular.module("authorityDirective", ["authSharedService"]).directive("access", [
    "AuthenticationService",
    function (AuthenticationService) {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                var grantedAuthorities = attrs.access.split(",");
                if (grantedAuthorities.length > 0) {
                    if (AuthenticationService.isAuthorized(grantedAuthorities)) {
                        element.removeClass("hide");
                    } else {
                        element.addClass("hide");
                    }
                }
            }
        };
    }
]);
