angular.module("authSharedService").factory("AuthenticationService",
    function ($http, $rootScope, authService, Session, $q, $resource, $base64, BACKEND_ENDPOINT) {
        return {
            login: function (username, password, rememberMe) {
                var encodedPassword = $base64.encode(password);
                var config = {
                    ignoreAuthModule: true,
                    cache: false
                };
                var data = $.param({
                    username: username,
                    password: encodedPassword,
                    rememberMe: rememberMe
                });

                function handleSuccess(response) {
                    authService.loginConfirmed(response.data);
                }

                function handleError(response) {
                    $rootScope.authenticationError = true;
                    Session.invalidate();
                }
                $http.post(BACKEND_ENDPOINT + "/authenticate?" + data, '', config).then(handleSuccess).catch(handleError);
            },

            isAuthorized: function (authorizedRoles) {
                if (!angular.isArray(authorizedRoles)) {
                    if (authorizedRoles === "*") {
                        return true;
                    }
                }
                var isAuthorized = false;
                angular.forEach(authorizedRoles, function (authorizedRole) {
                    if (Session.authorities.indexOf(authorizedRole) !== -1 || authorizedRole === "*") {
                        isAuthorized = true;
                    }
                });
                return isAuthorized;
            },
            getAccount: function () {
                $http.get(BACKEND_ENDPOINT + "/security/account").then(function (response) {
                    authService.loginConfirmed(response.data);
                });
            },
            logout: function () {
                $rootScope.authenticated = false;
                $rootScope.authenticationError = false;
                $rootScope.account = null;
                Session.invalidate();
                $http.get(BACKEND_ENDPOINT + "/logout");
                authService.loginCancelled();
            }
        };
    });
