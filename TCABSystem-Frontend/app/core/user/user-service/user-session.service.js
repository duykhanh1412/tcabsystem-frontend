angular.module("session").factory("Session", function () {
    this.create = function (data) {
        this.id = data.id;
        this.username = data.username;
        this.name = data.firstname + ' ' + data.lastname;
        this.email = data.email;
        this.authorities = [];
        angular.forEach(data.userroles, function (value, key) {
            this.push(value.name);
        }, this.authorities);
    };
    this.invalidate = function () {
        this.id = null;
        this.userName = null;
        this.authorities = null;
    };
    return this;
});