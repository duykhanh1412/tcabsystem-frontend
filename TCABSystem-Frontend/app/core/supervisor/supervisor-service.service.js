angular.module("supervisor.service").factory("Supervisor", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/supervisor", {}, {
        get: {
            method: 'GET',
            params: {

            },
            isArray: true
        }
    });
}]);
