angular.module("report.service").factory("Report", ["$resource", "BACKEND_ENDPOINT", function($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/report/:reportType/:teamId/:userId",{
        reportType: '@reportType',
        teamId: '@teamId',
        userId: '@userId'
    },{
        query: {
            method: 'GET',
            isArray: true
        }
    })
}]);