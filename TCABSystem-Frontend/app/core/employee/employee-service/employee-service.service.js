angular.module("employee.service").factory("User", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/employee/:employeeId", {
        employeeId: '@employeeId'
    }, {
        save: {
            method: 'POST',
            params: {}
        },
        delete: {
            method: 'PUT',
            params: {}
        },
        query: {
            method: 'GET',
            params: {},
            isArray: true
        },
        get: {
            method: 'GET',
            params: {}
        },
        update: {
            method: 'PUT',
            params: {}
        }
    });
}]);
