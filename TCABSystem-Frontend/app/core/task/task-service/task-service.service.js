angular.module("task.service").factory("Task", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/task/:taskId", {
        taskId: '@taskId'
    }, {
        save: {
            method: 'POST'
        }
    });
}]);
