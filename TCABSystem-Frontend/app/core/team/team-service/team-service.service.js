angular.module("team.service").factory("Team", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/team/:teamId/:userId", {
        teamId: '@teamId',
        userId: '@userId'
    }, {
        get: {
            method: 'GET',
            param: {
            }
        },
        save: {
            method: 'POST',
            isArray: true
        },
        delete: {
            method: 'DELETE'
        }
    });
}]);
