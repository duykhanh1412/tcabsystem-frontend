angular.module("teams.service").factory("Teams", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/teams/:teamId", {
        teamId: '@teamId'
    }, {
        get: {
            method: 'GET',
            isArray: true
        },
        save: {
            method: 'POST',
            isArray: true
        },
        delete: {
            method: 'DELETE'
        }
    });
}]);
