angular.module("unitSession").factory("UnitSession", function () {
    this.create = function (data) {
        this.id = data.id;
        this.name = data.name;
        this.unitcode = data.unitcode;
        this.startdate = data.startdate;
        this.enddate = data.enddate;
        this.description = data.description;
    };
    return this;
});