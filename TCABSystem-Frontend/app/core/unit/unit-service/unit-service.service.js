angular.module("unit.service").factory("Unit", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/unit/:unitId", {
        unitId: '@unitId'
    }, {
        query: {
            method: 'GET',
            params: {},
            isArray: true
        },
        save: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE'
        },
        search: {
            method: 'GET'
        }
    });
}]);
