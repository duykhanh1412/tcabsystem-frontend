angular.module("units.service").factory("Units", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/units/:userType/:id", {
        userType: '@userType',
        id: '@id'
    }, {
        query: {
            method: 'GET',
            params: {},
            isArray: true
        },
        save: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        delete: {
            method: 'DELETE'
        },
        search: {
            method: 'GET'
        }
    });
}]);
