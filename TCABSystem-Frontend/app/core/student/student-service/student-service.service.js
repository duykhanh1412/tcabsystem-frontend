angular.module("student.service").factory("Student", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/student/:studentId", {
        studentId: '@studentId'
    }, {
        save: {
            method: 'POST',
            params: {}
        },
        delete: {
            method: 'PUT',
            params: {}
        },
        query: {
            method: 'GET',
            params: {},
            isArray: true
        },
        get: {
            method: 'GET',
            params: {}
        },
        update: {
            method: 'PUT',
            params: {}
        }
    });
}]);
