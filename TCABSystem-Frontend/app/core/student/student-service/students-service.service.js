angular.module("students.service").factory("Students", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/students/:teamId/:projectId", {
        teamId: '@teamId',
        projectId: '@projectId'
    }, {
        save: {
            method: 'PUT',
            params: {
                
            },
            isArray: true
        },
        get: {
            method: 'GET',
            params: {
                name: ''
            },
            isArray: true
        }
    });
}]);
