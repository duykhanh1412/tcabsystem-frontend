angular.module("student.enrollment.service").factory("StudentEnrollment", ["$resource", "BACKEND_ENDPOINT", "getHeaderFilename", function ($resource, BACKEND_ENDPOINT, getHeaderFilename) {
    return $resource(BACKEND_ENDPOINT + "/student/enroll/:errorFile", {
        errorFile: '@errorFile'
    }, {
        save: {
            method: 'POST',
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        },
        get: {
            method: 'GET',
            responseType: 'arraybuffer',
            transformResponse: function (data, headers) {
                return {
                    data: data,
                    filename: getHeaderFilename(headers)
                };
            }
        }
    });
}]);
