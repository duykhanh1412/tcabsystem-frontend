angular.module("project.service").factory("Project", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/project/:projectId", {
        projectId: '@projectId'
    }, {
        save: {
            method: 'POST',
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        },
        query: {
            method: 'GET',
            params: {},
            isArray: true
        },
        get: {
            method: 'GET',
            params: {}
        },
        delete: {
            method: 'DELETE',
            params: {}
        },
        update: {
            method: 'PUT',
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }
    });
}]);
