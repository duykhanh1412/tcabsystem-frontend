angular.module("project.allocation.service").factory("ProjectAllocation", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/project/allocation/:projectId", {
        projectId: '@projectId'
    }, {
        save: {
            method: 'POST',
            isArray: true
        },
        get: {
            method: 'GET',
            isArray: true
        },
        update: {
            method: 'DELETE',
        }
    });
}]);
