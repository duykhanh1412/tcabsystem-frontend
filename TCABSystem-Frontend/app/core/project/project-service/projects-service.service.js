angular.module("projects.service").factory("Projects", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/projects/:unitId", {}, {
        get: {
            method: 'GET',
            params: {
                
            },
            isArray: true
        }
    });
}]);
