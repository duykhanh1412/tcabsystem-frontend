angular.module("projectRole.service").factory("ProjectRole", ["$resource", "BACKEND_ENDPOINT", function ($resource, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/role/:id", {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            isArray: true
        }
    });
}]);
