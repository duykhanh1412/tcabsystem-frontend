angular.module("header.service").service("getHeaderFilename", function () {
    return function (headers) {
        var header = headers('content-disposition');
        var result = header.split(';')[1].trim().split('=')[1];
        return result.replace(/"/g, '');
    };
});