angular.module("description.service").factory("ProjectDescription", ["$resource", "getHeaderFilename", "BACKEND_ENDPOINT", function ($resource, getHeaderFilename, BACKEND_ENDPOINT) {
    return $resource(BACKEND_ENDPOINT + "/project/description/:projectId", {
        projectId: '@projectId'
    }, {
        get: {
            method: 'GET',
            responseType: 'arraybuffer',
            transformResponse: function (data, headers) {
                return {
                    data: data,
                    filename: getHeaderFilename(headers)
                };
            }
        }
    });
}]);
