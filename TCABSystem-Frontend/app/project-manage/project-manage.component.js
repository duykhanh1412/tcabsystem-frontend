angular.module("manageProject").component("manageProject", {
    templateUrl: "tcabs/projectManage.html",
    controller: ["ProjectDescription", "Units", "Project", "$location", "Session", function (ProjectDescription, Units, Project, $location, Session) {
        var self = this;
        self.isResponseEmpty = false;
        self.isServerFailed = false;
        self.isProjectAllocatedToATeam = false;
        self.deleteSucessful = false;
        self.projectName = '';
        self.unitId = 0;
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });
        self.selectedproject = new Project();
        self.projects = [];
        self.search = function (projectName, unitId) {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isProjectAllocatedToATeam = false;
            self.deleteSucessful = false;
            self.projects = Project.query({
                projectName: projectName,
                unitId: unitId
            }, function (response, status) {
                self.isResponseEmpty = false;
                self.isServerFailed = false;
            }, function (error, status) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
            });
        };
        self.download = function (index) {
            self.projectId = self.projects[index].id;
            ProjectDescription.get({
                projectId: self.projectId
            }).$promise.then(function (response) {
                self.url = URL.createObjectURL(new Blob([response.data]));
                self.a = document.createElement('a');
                self.a.href = self.url;
                self.a.download = response.filename;
                self.a.target = '_blank';
                self.a.click();
            }).catch(function (error) {

            });
        };
        self.delete = function (index) {
            if (confirm("Are you sure you want to delete this project?")) {
                self.isServerFailed = false;
                self.deleteSucessful = false;
                self.selectedproject.id = self.projects[index].id;
                self.selectedproject.$delete({
                    projectId: self.selectedproject.id
                }).then(function (response) {
                    self.projects.splice(index, 1);
                    self.isProjectAllocatedToATeam = false;
                    self.deleteSucessful = true;
                }).catch(function (error, status) {
                    if (error.status === 400) {
                        self.isProjectAllocatedToATeam = true;
                    }
                    if (error.status === 500) {
                        self.isServerFailed = true;
                    }
                });
            }
        };
        self.update = function (index) {
            $location.path("/updateProject/" + self.projects[index].id).replace();
        };
    }]
});
