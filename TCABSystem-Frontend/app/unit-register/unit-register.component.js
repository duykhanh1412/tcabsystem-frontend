angular.module("registerUnit").component("registerUnit", {
    templateUrl: "tcabs/unitRegister.html",
    controller: ["Unit", function (Unit) {
        var self = this;
        self.isServerFailed = false;
        self.isUnitRegisteredSuccessfully = false;
        self.isInputMissing = false;
        self.isUnitAlreadyExisting = false;
        self.unit = new Unit();

        self.register = function () {
            self.isServerFailed = false;
            self.isUnitRegisteredSuccessfully = false;
            self.isInputMissing = false;
            self.isUnitAlreadyExisting = false;
            self.unit.$save().then(function (response) {
                self.isUnitRegisteredSuccessfully = true;
                self.unit.startdate = new Date(response.startdate);
                self.unit.enddate = new Date(response.enddate);
            }).catch(function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 406) {
                    self.isUnitAlreadyExisting = true;
                }
                if (error.status === 400) {
                    self.isInputMissing = true;
                }
            });
        };
    }]
});
