angular.module("manageAllocation").component("manageAllocation", {
    templateUrl: "tcabs/projectAllocationManage.html",
    controller: ["ProjectAllocation", "Projects", "Units", "$location", "Session", function (ProjectAllocation, Projects, Units, $location, Session) {
        var self = this;
        self.isServerFailed = false;
        self.isResponseEmpty = false;
        self.isTeamAlreadyMeetSupervisor = false;
        self.isProjectAllocationUpdatedSuccessfully = false;
        self.units = [];
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });
        self.project;
        self.projects = [];
        self.teams = [];
        self.getProjects = function () {
            self.isServerFailed = false;
            self.projects = Projects.get({
                unitId: self.selectedUnit
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };

        self.search = function (projectId) {
            self.isServerFailed = false;
            self.isResponseEmpty = false;
            self.isProjectAllocationUpdatedSuccessfully = false;
            self.teams = [];
            self.projectAllocations = ProjectAllocation.get({
                projectId: projectId
            }, function (response) {
                self.teams = response;
            }, function (error) {
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
            });
        };

        self.delete = function (index) {
            self.isServerFailed = false;
            self.isResponseEmpty = false;
            self.isTeamAlreadyMeetSupervisor = false;
            self.isProjectAllocationUpdatedSuccessfully = false;
            ProjectAllocation.update({
                projectId: self.teams[index].teamId
            }, function (reponse) {
                self.teams.splice(index, 1);
                self.isProjectAllocationUpdatedSuccessfully = true;
            }, function (error) {
                if (error.status === 406) {
                    self.isTeamAlreadyMeetSupervisor = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
            });
        };

        self.update = function (index) {
            $location.path("/updateAllocation/" + self.teams[index].teamId).replace();
        }
    }]
});
