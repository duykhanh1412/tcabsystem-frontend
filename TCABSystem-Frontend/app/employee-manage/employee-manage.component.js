angular.module("manageEmployee").component("manageEmployee", {
    templateUrl: "tcabs/employeeManage.html",
    controller: ["User", "$location", function (User, $location) {
        var self = this;
        self.isResponseEmpty = false;
        self.isServerFailed = false;
        self.isEmployeeInvolveInAnyUnit = false;
        self.isAdminEmpty = false;
        self.deleteSucessful = false;
        self.searchUser = new User();
        self.name = '';
        self.email = '';
        self.users = [];
        self.managedUser = new User();
        self.search = function () {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isEmployeeInvolveInAnyUnit = false;
            self.isAdminEmpty = false;
            self.deleteSucessful = false;
            self.users = User.query({
                name: self.name,
                email: self.email
            }, function (response) {
                self.isResponseEmpty = false;
                self.isServerFailed = false;
            }, function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
            });
        };
        self.delete = function (index) {
            if (confirm("Are you sure you want to delete this employee?")) {
                self.isServerFailed = false;
                self.deleteSucessful = false;
                self.managedUser.id = self.users[index].id;
                self.managedUser.email = self.users[index].email;
                self.managedUser.userroles = self.users[index].userroles;
                self.managedUser.$delete().then(function (response) {
                    self.users.splice(index, 1);
                    self.isEmployeeInvolveInAnyUnit = false;
                    self.isAdminEmpty = false;
                    self.deleteSucessful = true;
                }).catch(function (error, status) {
                    if (error.status === 400) {
                        self.isEmployeeInvolveInAnyUnit = true;
                    }
                    if (error.status === 406) {
                        self.isAdminEmpty = true;
                    }
                    if (error.status === 500) {
                        self.isServerFailed = true;
                    }
                });
            }
        };
        self.update = function (index) {
            $location.path("/updateEmployee/" + self.users[index].id).replace();
        };
    }]
});
