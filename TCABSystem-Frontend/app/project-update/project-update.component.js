angular.module("updateProject").component("updateProject", {
    templateUrl: "tcabs/projectUpdate.html",
    controller: ["Project", "Units", "$routeParams", "Session", function (Project, Units, $routeParams, Session) {
        var self = this;
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });
        self.isProjectNameAlreadyExisting = false;
        self.isRequiredFieldsEmpty = false;
        self.isProjectUpdated = false;
        self.isFileSizeTooLarge = false;
        self.project = Project.get({
            projectId: $routeParams.projectId
        }, function (response) {
            angular.forEach(self.units, function (unit) {
                if (self.project.unit.id === unit.id) {
                    self.project.unit = unit;
                }
            });
            self.project.description = null;
        }, function (error, status) {

        });

        self.update = function (projectId) {
            self.isProjectNameAlreadyExisting = false;
            self.isRequiredFieldsEmpty = false;
            self.isProjectUpdated = false;
            self.isFileSizeTooLarge = false;
            self.formData = new FormData();
            self.formData.append("projectName", self.project.name);
            self.formData.append("unit", self.project.unit.id);
            self.formData.append("startdate", self.project.unit.startdate);
            self.formData.append("enddate", self.project.unit.enddate);
            self.formData.append("maximunmembers", self.project.maximunmembers);
            self.formData.append("description", self.project.description);
            self.formData.append("budget", self.project.budget);
            self.formData.append("projectId", self.project.id);
            Project.update({}, self.formData).$promise.then(function (response) {
                self.isProjectUpdated = true;
            }).catch(function (error) {
                if (error.status === 302) {
                    self.isProjectNameAlreadyExisting = true;
                }
                if (error.status === 500 || error.status === 400) {
                    self.isRequiredFieldsEmpty = true;
                }
                if (error.status === 406) {
                    self.isFileSizeTooLarge = true;
                }
            });
        };
    }]
});
