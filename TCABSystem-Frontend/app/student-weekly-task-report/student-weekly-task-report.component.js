angular.module("reportStudentWeeklyTask").component("reportStudentWeeklyTask", {
    templateUrl: "tcabs/studentWeeklyTaskReport.html",
    controller: ["Report", "Units", "Projects", "Teams", "Students", "Session", function (Report, Units, Projects, Teams, Students, Session) {
        var self = this;
        self.isServerFailed = false;
        self.isResponseEmpty = false;
        self.isProjectNotChoosed = false;
        self.isTeamNotChoosed = false;
        self.isTeamNameExisting = false;
        self.isTeamNameNotExisting = false;
        self.isStudentNameExisting = false;
        self.isStudentNameNotExisting = false;
        self.isSearchSuccessfull = false;
        self.unit = {};
        self.units = [];
        self.project = {};
        self.projects = [];
        self.teamName = "";
        self.team = [];
        self.teams = [];
        self.week;
        self.weeks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
        self.studentName = "";
        self.studentTasks = [];
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });

        self.getProjects = function () {
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.projects = Projects.get({
                unitId: self.unit.id
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };

        self.searchTeams = function () {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.isTeamNameExisting = false;
            self.isTeamNameNotExisting = false;
            if (self.project == null) {
                self.isProjectNotChoosed = true;
                return;
            }
            self.teams = Teams.get({
                teamName: self.teamName,
                projectId: self.project.id
            }, function (response) {
                self.isServerFailed = false;
                self.isTeamNameNotExisting = false;
                self.isTeamNameExisting = true;
            }, function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 400) {
                    self.isProjectNotChoosed = true;
                }
            });
        };

        self.selectTeam = function () {
            self.isTeamNameExisting = false;
            self.teamName = self.team[0].name;
        };

        self.search = function () {
            self.isServerFailed = false;
            self.isResponseEmpty = false;
            self.isSearchSuccessfull = false;
            self.studentTasks = Report.query({
                reportType: 'weeklyStudentTask',
                teamId: self.team[0].id,
                week: self.week
            }, function (response) {
                self.isSearchSuccessfull = true;
            }, function (error) {
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
            });
        };
    }]
});
