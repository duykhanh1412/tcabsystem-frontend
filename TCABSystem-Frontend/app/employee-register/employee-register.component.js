angular.module("registerEmployee").component("registerEmployee", {
    templateUrl: "tcabs/employeeRegister.html",
    controller: ["User", "$location", function (User, $location) {
        var self = this;
        self.isUserAlreadyExisting = false;
        self.isRequiredFieldsEmpty = false;
        self.convener = false;
        self.supervisor = false;
        self.admin = false;
        self.user = new User();

        self.register = function () {
            self.isUserAlreadyExisting = false;
            self.isRequiredFieldsEmpty = false;
            self.isUserCreated = false;
            if (self.convener || self.admin || self.supervisor) {
                self.user.userroles = [];
            }

            if (self.admin) {
                self.user.userroles.push({
                    id: 1,
                    name: 'ADMIN'
                });
            }
            if (self.convener) {
                self.user.userroles.push({
                    id: 2,
                    name: 'CONVENER'
                });
            }
            if (self.supervisor) {
                self.user.userroles.push({
                    id: 3,
                    name: 'SUPERVISOR'
                });
            }

            self.user.$save().then(function (response) {
                self.user.dob = new Date(response.dob);
                self.isUserCreated = true;
            }).catch(function (error, status) {
                if (error.status === 302) {
                    self.isUserAlreadyExisting = true;
                }
                if (error.status === 500) {
                    self.isRequiredFieldsEmpty = true;
                }
                if (error.status === 400) {
                    self.isRequiredFieldsEmpty = true;
                }
            });
        };
    }]
});
