angular.module("updateAllocation").component("updateAllocation", {
    templateUrl: "tcabs/projectAllocationUpdate.html",
    controller: ["ProjectAllocation", "Team", "$routeParams", function (ProjectAllocation, Team, $routeParams) {
        var self = this;
        self.isTeamMissing = false;
        self.isSupervisorIdEmpty = false;
        self.isTeamAlreadyMeetSupervisor = false;
        self.isTeamIdInvalid = [{
            teamCount: false
        }];
        self.isTeamNumberLargerThanAllowed = [{
            teamCount: false
        }];
        self.isProjectAllocatedSuccessfully = false;
        self.isServerFailed = false;
        self.isProjectNotChoosed = false;
        self.teams = [{
            id: 0
        }];
        self.supervisor = {};
        self.currentTeam = {};
        self.currentProject = {};
        self.project = Team.get({
            teamId: $routeParams.teamId
        }, function (response) {
            self.supervisor.id = self.project.supervisorId;
            self.currentProject.id = self.project.projectId;
            self.currentProject.maximunmembers = self.project.maximunmembers;
            self.currentTeam = self.project.teamId;
        }, function (error) {
            self.isServerFailed = true;
        });

        self.allocate = function () {
            self.isProjectNotChoosed = false;
            self.isTeamMissing = false;
            self.isSupervisorIdEmpty = false;
            angular.forEach(self.isTeamIdInvalid, function (invalidTeam) {
                invalidTeam.teamCount = false;
            });
            angular.forEach(self.isTeamNumberLargerThanAllowed, function (invalidTeam) {
                invalidTeam.teamCount = false;
            });
            self.isProjectAllocatedSuccessfully = false;
            self.isTeamCountLargerThanAllowed = false;
            self.isServerFailed = false;
            if (self.teams.length === 0) {
                self.isTeamMissing = true;
            }
            if (typeof self.project == 'undefined') {
                self.isProjectNotChoosed = true;
                return;
            }
            if (typeof self.supervisor == 'undefined') {
                self.isSupervisorIdEmpty = true;
                return;
            }
            self.teams[0].project = self.currentProject;
            self.teams[0].supervisor = self.supervisor;
            self.teams[0].teamcount = 0;
            self.teams[0].id = self.currentTeam;
            ProjectAllocation.save({}, self.teams).$promise.then(function (response) {
                self.isProjectAllocatedSuccessfully = true;
            }).catch(function (error) {
                if (error.status === 400) {
                    self.index = 0;
                    angular.forEach(self.teams, function (team) {
                        angular.forEach(error.data, function (invalidTeam) {
                            if (team.id == invalidTeam.id) {
                                if (invalidTeam.teamcount > invalidTeam.project.maximunmembers) {
                                    self.isTeamNumberLargerThanAllowed[self.index].teamCount = true;
                                } else {
                                    self.isTeamIdInvalid[self.index].teamCount = true;
                                }
                            }
                        });
                        self.index = self.index + 1;
                    });
                }
                if (error.status === 404) {
                    self.isSupervisorIdEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.stats === 406) {
                    self.isTeamAlreadyMeetSupervisor = true;
                }
            });
        };
    }]
});
