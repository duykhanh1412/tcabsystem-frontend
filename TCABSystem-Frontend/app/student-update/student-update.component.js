angular.module("updateStudent").component("updateStudent", {
    templateUrl: "tcabs/studentUpdate.html",
    controller: ["Student", "$routeParams", function (Student, $routeParams) {
        var self = this;
        self.isEmailAlreadyExisting = false;
        self.isRequiredFieldsEmpty = false;
        self.isStudentUpdated = false;
        self.student = Student.get({
            studentId: $routeParams.studentId
        }, function (response) {
            self.student.dob = new Date(response.dob);
        }, function (error, status) {

        });

        self.update = function (studentId) {
            self.isEmailAlreadyExisting = false;
            self.isRequiredFieldsEmpty = false;
            self.isStudentUpdated = false;

            self.student.$update({
                studentId: studentId
            }).then(function (response) {
                self.student.dob = new Date(response.dob);
                self.isStudentUpdated = true;
            }).catch(function (error, status) {
                if (error.status === 302) {
                    self.isEmailAlreadyExisting = true;
                }
                if (error.status === 500 || error.status === 400) {
                    self.isRequiredFieldsEmpty = true;
                }
            });
        };
    }]
});
