angular.module("manageTeam").component("manageTeam", {
    templateUrl: "tcabs/teamManage.html",
    controller: ["Teams", "Team", "Projects", "Units", "$location", "Session", function (Teams, Team, Projects, Units, $location, Session) {
        var self = this;
        self.isResponseEmpty = false;
        self.isServerFailed = false;
        self.isTeamNotEmpty = false;
        self.deleteSucessful = false;
        self.unitId = 0;
        self.teamName = '';
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });
        self.selectedTeam = new Team();
        self.project = {};
        self.teams = [];
        self.projects = [];
        self.getProjects = function () {
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.projects = Projects.get({
                unitId: self.selectedUnit
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };
        self.search = function (teamName, projectId) {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isTeamNotEmpty = false;
            self.deleteSucessful = false;
            self.isProjectNotChoosed = false;
            if (self.project == null) {
                self.isProjectNotChoosed = true;
            }
            self.teams = Teams.get({
                teamName: teamName,
                projectId: projectId
            }, function (response) {
                self.isResponseEmpty = false;
                self.isServerFailed = false;
            }, function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
                if (error.status === 400) {
                    self.isProjectNotChoosed = true;
                }
            });
        };

        self.delete = function (index) {
            if (confirm("Are you sure you want to delete this team?")) {
                self.isServerFailed = false;
                self.deleteSucessful = false;

                Team.delete({
                    teamId: self.teams[index].id
                }, function (response) {
                    self.teams.splice(index, 1);
                    self.isTeamNotEmpty = false;
                    self.deleteSucessful = true;
                }, function (error) {
                    if (error.status === 406) {
                        self.isTeamNotEmpty = true;
                    }
                    if (error.status === 500) {
                        self.isServerFailed = true;
                    }
                });
            }
        };
        self.update = function (index) {
            $location.path("/updateTeam/" + self.teams[index].id).replace();
        };
    }]
});
