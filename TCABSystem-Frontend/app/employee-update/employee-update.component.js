angular.module("updateEmployee").component("updateEmployee", {
    templateUrl: "tcabs/employeeUpdate.html",
    controller: ["User", "$routeParams", function (User, $routeParams) {
        var self = this;
        self.isEmailAlreadyExisting = false;
        self.isRequiredFieldsEmpty = false;
        self.isAdminEmpty = false;
        self.isUserUpdated = false;
        self.convener = false;
        self.supervisor = false;
        self.admin = false;
        self.user = User.get({
            employeeId: $routeParams.employeeId
        }, function (response) {
            angular.forEach(self.user.userroles, function (userrole) {
                switch (userrole.name) {
                    case "ADMIN":
                        self.admin = true;
                        break;
                    case "CONVENER":
                        self.convener = true;
                        break;
                    case "SUPERVISOR":
                        self.supervisor = true;
                        break;
                }
            });
            self.user.dob = new Date(response.dob);
        }, function (error, status) {

        });

        self.update = function (employeeId) {
            self.isEmailAlreadyExisting = false;
            self.isRequiredFieldsEmpty = false;
            self.isAdminEmpty = false;
            self.isUserUpdated = false;

            self.user.userroles = [];
            if (self.admin) {
                self.user.userroles.push({
                    id: 1,
                    name: 'ADMIN'
                });
            }
            if (self.convener) {
                self.user.userroles.push({
                    id: 2,
                    name: 'CONVENER'
                });
            }
            if (self.supervisor) {
                self.user.userroles.push({
                    id: 3,
                    name: 'SUPERVISOR'
                });
            }

            self.user.$update({
                employeeId: employeeId
            }).then(function (response) {
                self.user.dob = new Date(response.dob);
                self.isUserUpdated = true;
            }).catch(function (error, status) {
                if (error.status === 302) {
                    self.isEmailAlreadyExisting = true;
                }
                if (error.status === 500 || error.status === 400) {
                    self.isRequiredFieldsEmpty = true;
                }
                if (error.status === 406) {
                    self.isAdminEmpty = true;
                }
            });
        };
    }]
});
