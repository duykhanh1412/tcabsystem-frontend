angular.module("submitTask").component("submitTask", {
    templateUrl: "tcabs/taskSubmit.html",
    controller: ["Team", "Units", "Projects", "Task", "ProjectRole", "Session", function (Team, Units, Projects, Task, ProjectRole, Session) {
        var self = this;
        self.isTaskInvalid = [{
            teamCount: false
        }];
        self.isTaskSubmittedForThisWeek = false;
        self.isTaskSubmittedSuccessfully = false;
        self.isServerFailed = false;
        self.isProjectNotChoosed = false;
        self.isStudentNotEnrolledInTheProject = false;
        self.units = [];
        self.units = Units.query({
            userType: 'student',
            id: Session.id
        });
        self.project;
        self.selectedUnit = 0;
        self.projects = [];
        self.projectRoles = [];
        self.week;
        self.student = new Task();
        self.student.user = {};
        self.student.studentTasks = [{
            task: {
                id: 0
            }
        }];
        self.team = {};
        self.getProjects = function () {
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.projects = Projects.get({
                unitId: self.selectedUnit
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };
        self.getProjectRoles = function () {
            self.isStudentNotEnrolledInTheProject = false;
            self.projectRoles = ProjectRole.get({
                id: self.project.id
            }, function (response) {

            }, function (error) {

            });
            self.team = Team.get({
                teamId: self.project.id,
                userId: Session.id
            }, function (response) {

            }, function (error) {
                if(error.status === 404) {
                    self.isStudentNotEnrolledInTheProject = true;
                }
            });
        };
        self.addStudentTask = function () {
            self.isProjectNotChoosed = false;
            if (typeof self.project == 'undefined') {
                self.isProjectNotChoosed = true;
                return;
            }
            var pointer = self.student.studentTasks.length - 1;
            self.student.studentTasks.push({
                task: {
                    id: self.student.studentTasks[pointer].task.id + 1
                }
            });
            self.isTaskInvalid.push({
                teamCount: false
            });
        };
        self.removeStudentTask = function (index) {
            self.student.studentTasks.splice(index, 1);
            self.isTaskInvalid.splice(index, 1);
        };

        self.submitTask = function () {
            self.isProjectNotChoosed = false;
            angular.forEach(self.isTaskInvalid, function (invalidStudentTask) {
                invalidStudentTask.teamCount = false;
            });
            self.isTaskSubmittedSuccessfully = false;
            self.isTaskSubmittedForThisWeek = false;
            self.isServerFailed = false;
            if (typeof self.project == 'undefined') {
                self.isProjectNotChoosed = true;
                return;
            }
            self.student.user.id = Session.id;
            angular.forEach(self.student.studentTasks, function (studentTask) {
                studentTask.week = self.week;
                studentTask.team = self.team;
            });
            self.student.$save({

            }).then(function (response) {
                console.log(response);
                self.isTaskSubmittedSuccessfully = true;
                var indexStudentTask = 0;
                angular.forEach(response.studentTasks, function (studentTask) {
                    self.student.studentTasks[indexStudentTask].date = new Date(studentTask.date);
                    indexStudentTask = indexStudentTask + 1;
                });
            }).catch(function (error) {
                if (error.status === 400) {
                    self.index = 0;
                    angular.forEach(self.student.studentTasks, function (studentTask) {
                        angular.forEach(error.data.studentTasks, function (invalidStudentTask) {
                            if (studentTask.task.id == invalidStudentTask.task.id) {
                                self.isTaskInvalid[self.index].teamCount = true;
                            }
                        });
                        self.index = self.index + 1;
                    });
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 302) {
                    self.isTaskSubmittedForThisWeek = true;
                }
            });
        };
    }]
});
