angular.module("manageStudent").component("manageStudent", {
    templateUrl: "tcabs/studentManage.html",
    controller: ["Student", "$location", function (Student, $location) {
        var self = this;
        self.isResponseEmpty = false;
        self.isServerFailed = false;
        self.isStudentInvolveInAnyUnit = false;
        self.deleteSucessful = false;
        self.searchStudent = new Student();
        self.name = '';
        self.email = '';
        self.students = [];
        self.managedStudent = new Student();
        self.search = function () {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isStudentInvolveInAnyUnit = false;
            self.deleteSucessful = false;
            self.students = Student.query({
                name: self.name,
                email: self.email
            }, function (response, status) {
                self.isResponseEmpty = false;
                self.isServerFailed = false;
            }, function (error, status) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
            });
        };
        self.delete = function (index) {
            if (confirm("Are you sure you want to delete this student?")) {
                self.isServerFailed = false;
                self.deleteSucessful = false;
                self.managedStudent.id = self.students[index].id;
                self.managedStudent.email = self.students[index].email;
                self.managedStudent.userroles = self.students[index].userroles;
                self.managedStudent.$delete().then(function (response) {
                    self.students.splice(index, 1);
                    self.isStudentInvolveInAnyUnit = false;
                    self.deleteSucessful = true;
                }).catch(function (error, status) {
                    if (error.status === 400) {
                        self.isStudentInvolveInAnyUnit = true;
                    }
                    if (error.status === 500) {
                        self.isServerFailed = true;
                    }
                });
            }
        };
        self.update = function (index) {
            $location.path("/updateStudent/" + self.students[index].id).replace();
        };
    }]
});
