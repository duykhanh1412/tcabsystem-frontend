angular.module("updateTeam").component("updateTeam", {
    templateUrl: "tcabs/teamUpdate.html",
    controller: ["Team", "Students", "$routeParams", function (Team, Students, $routeParams) {
        var self = this;
        self.isTeamMissing = false;
        self.isTeamNameInvalid = false;
        self.isTeamNumberLargerThanAllowed = [{
            teamCount: false
        }];
        self.isStudentIdInvalid = [];
        self.isProjectAllocatedSuccessfully = false;
        self.isServerFailed = false;
        self.supervisor = {};
        self.selectedUnit = {};
        self.currentProject = {};
        self.teamName = "";
        self.isTeamNumberLargerThanAllowed = false;
        self.teams = [{
            id: 0
        }];
        self.team = new Team();
        self.isStudentNameExisting = [false];
        self.isStudentNameNotExisting = [false];
        self.studentName = [];
        self.selectedStudent = [];
        self.studentOption = [];
        self.unitName = '';
        self.projectName = '';
        self.supervisorName = '';
        self.students = [{
            id: 0
        }];
        self.team = Team.get({
            teamIdentity: $routeParams.teamId
        }, function (response) {
            self.selectedUnit = self.team.project.unit;
            self.supervisor = self.team.supervisor;
            self.currentProject = self.team.project;
            self.teamName = self.team.name;
            self.unitName = self.selectedUnit.unitcode + ' - ' + self.selectedUnit.name;
            self.projectName = self.currentProject.id + ' - ' + self.currentProject.name;
            self.supervisorName = self.supervisor.id + ' - ' + self.supervisor.user.firstname + ' ' + self.supervisor.user.lastname;
            if (self.team.students.length > 0) {
                self.students = self.team.students;
                self.increment = 0;
                angular.forEach(self.students, function (student) {
                    self.studentName[self.increment] = student.id + ' - ' + student.user.firstname + ' ' + student.user.lastname;
                    self.increment++;
                    self.isStudentIdInvalid.push({
                        teamCount: false
                    });
                });
            }
        }, function (error) {
            self.isServerFailed = true;
        });

        self.searchedStudents = [];

        self.searchStudents = function (index) {
            self.isStudentNameExisting[index] = false;
            self.isStudentNameNotExisting[index] = false;
            self.searchedStudents[index] = Students.get({
                name: self.studentName[index],
                unitId: self.selectedUnit.id
            }, function (response) {
                self.isStudentNameExisting[index] = true;
            }, function (error) {
                if (error.status === 404) {
                    self.isStudentNameNotExisting[index] = true;
                }
            });
        };

        self.selectStudents = function (student, index) {
            self.students[index] = student[0];
            self.studentName[index] = student[0].id + ' - ' + student[0].user.firstname + ' ' + student[0].user.lastname;
            self.isStudentNameExisting[index] = false;
            self.isStudentNameNotExisting[index] = false;
        };

        self.addStudent = function () {
            self.isTeamNameInvalid = false;
            self.students.push({
                id: 0
            });
            self.isStudentIdInvalid.push({
                teamCount: false
            });
            self.isStudentNameExisting.push(false);
            self.isStudentNameNotExisting.push(false);
            self.studentName.push("");
        };
        self.removeStudent = function (index) {
            self.isTeamNumberLargerThanAllowed = false;
            self.students.splice(index, 1);
            self.isStudentIdInvalid.splice(index, 1);
            self.isStudentNameExisting.splice(index, 1);
            self.isStudentNameNotExisting.splice(index, 1);
            self.studentName.splice(index, 1);
        };

        self.allocate = function () {
            self.isTeamMissing = false;
            self.isTeamNameInvalid = false;
            self.isTeamNumberLargerThanAllowed = false;
            angular.forEach(self.isStudentIdInvalid, function (invalidTeam) {
                invalidTeam.teamCount = false;
            });
            self.isProjectAllocatedSuccessfully = false;
            self.isTeamCountLargerThanAllowed = false;
            self.isServerFailed = false;
            if (self.teams.length === 0) {
                self.isTeamMissing = true;
            }
            if (self.students.length > self.currentProject.maximunmembers) {
                self.isTeamNumberLargerThanAllowed = true;
                return;
            }
            if (self.teamName === "") {
                self.isTeamMissing = true;
                return;
            }
            self.teams[0].teamcount = self.students.length;
            self.teams[0].students = self.students;
            self.teams[0].name = self.teamName;
            self.teams[0].project = self.currentProject;
            self.teams[0].supervisor = self.supervisor;
            self.teams[0].id = self.team.id;
            Team.save({}, self.teams).$promise.then(function (response) {
                self.isProjectAllocatedSuccessfully = true;
            }).catch(function (error) {
                if (error.status === 400) {
                    self.index = 0;
                    if (error.data[0].students.length == 0) {
                        self.isTeamNameInvalid = true;
                    }
                    if (error.data[0].teamcount > error.data[0].project.maximunmembers) {
                        self.isTeamNumberLargerThanAllowed = true;
                    }
                    angular.forEach(self.teams[0].students, function (student) {
                        angular.forEach(error.data[0].students, function (invalidStudent) {
                            if (student.id == invalidStudent.id) {
                                self.isStudentIdInvalid[self.index].teamCount = true;
                            }
                        });
                        self.index = self.index + 1;
                    });
                }
                if (error.status === 404) {
                    self.isSupervisorIdEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.stats === 406) {
                    self.isTeamAlreadyMeetSupervisor = true;
                }
            });
        };
    }]
});
