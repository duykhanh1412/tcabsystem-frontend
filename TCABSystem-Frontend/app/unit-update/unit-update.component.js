angular.module("updateUnit").component("updateUnit", {
    templateUrl: "tcabs/unitUpdate.html",
    controller: ["Unit", "UnitSession", function (Unit, UnitSession) {
        var self = this;
        self.isServerFailed = false;
        self.isUnitUpdatedSuccessfully = false;
        self.isInputMissing = false;
        self.isUnitAlreadyExisting = false;
        self.unit = new Unit();
        self.unit.name = UnitSession.name;
        self.unit.unitcode = UnitSession.unitcode;
        self.unit.description = UnitSession.description;
        self.unit.startdate = new Date(UnitSession.startdate);
        self.unit.enddate = new Date(UnitSession.enddate);

        self.update = function () {
            self.isServerFailed = false;
            self.isUnitUpdatedSuccessfully = false;
            self.isInputMissing = false;
            self.isUnitAlreadyExisting = false;
            self.unit.$update().then(function (response) {
                self.isUnitUpdatedSuccessfully = true;
                self.unit.startdate = new Date(response.startdate);
                self.unit.enddate = new Date(response.enddate);
            }).catch(function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 406) {
                    self.isUnitAlreadyExisting = true;
                }
                if (error.status === 400) {
                    self.isInputMissing = true;
                }
            });
        };
    }]
});
