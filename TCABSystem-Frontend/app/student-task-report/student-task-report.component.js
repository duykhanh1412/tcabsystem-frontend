angular.module("reportStudentTask").component("reportStudentTask", {
    templateUrl: "tcabs/studentTaskReport.html",
    controller: ["Report", "Units", "Projects", "Teams", "Students", "Session", function (Report, Units, Projects, Teams, Students, Session) {
        var self = this;
        self.isServerFailed = false;
        self.isResponseEmpty = false;
        self.isProjectNotChoosed = false;
        self.isTeamNotChoosed = false;
        self.isTeamNameExisting = false;
        self.isTeamNameNotExisting = false;
        self.isStudentNameExisting = false;
        self.isStudentNameNotExisting = false;
        self.isSearchSuccessfull = false;
        self.unit = {};
        self.units = [];
        self.project = {};
        self.projects = [];
        self.teamName = "";
        self.team = [];
        self.teams = [];
        self.student = [];
        self.students = [];
        self.studentName = "";
        self.studentTasks = [];
        self.totalTimeTaken = 0;
        self.totalCost = 0;
        self.totalTimeTakenPerWeek = 0;
        self.costPerWeek = 0;
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });

        self.getProjects = function () {
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.projects = Projects.get({
                unitId: self.unit.id
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };

        self.searchTeams = function () {
            self.isResponseEmpty = false;
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.isTeamNameExisting = false;
            self.isTeamNameNotExisting = false;
            if (self.project == null) {
                self.isProjectNotChoosed = true;
                return;
            }
            self.teams = Teams.get({
                teamName: self.teamName,
                projectId: self.project.id
            }, function (response) {
                self.isServerFailed = false;
                self.isTeamNameNotExisting = false;
                self.isTeamNameExisting = true;
            }, function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 400) {
                    self.isProjectNotChoosed = true;
                }
            });
        };

        self.selectTeam = function () {
            self.isTeamNameExisting = false;
            self.teamName = self.team[0].name;
        };

        self.searchStudents = function () {
            self.isServerFailed = false;
            self.isTeamNotChoosed = false;
            self.isStudentNameExisting = false;
            self.isStudentNameNotExisting = false;
            if (self.team[0] == null) {
                self.isTeamNotChoosed = true;
                return;
            }
            self.students = Students.get({
                teamId: self.team[0].id,
                projectId: self.project.id,
                name: self.studentName
            }, function (response) {
                self.isServerFailed = false;
                self.isStudentNameNotExisting = false;
                self.isStudentNameExisting = true;
            }, function (error) {
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.status === 406) {
                    self.isProjectNotChoosed = true;
                    self.isTeamNotChoosed = true;
                }
            });
        };

        self.selectStudent = function () {
            self.isStudentNameExisting = false;
            self.studentName = self.student[0].id + ' - ' + self.student[0].user.firstname + ' ' + self.student[0].user.lastname;
        };

        self.search = function () {
            self.isServerFailed = false;
            self.isResponseEmpty = false;
            self.isSearchSuccessfull = false;
            self.studentTasks = Report.query({
                reportType: 'studentTask',
                teamId: self.team[0].id,
                userId: self.student[0].user.id
            }, function (response) {
                self.isSearchSuccessfull = true;
                angular.forEach(self.studentTasks, function (studentTask) {
                    self.totalTimeTaken += studentTask.timeTaken;
                    self.totalCost += studentTask.totalCost;
                });
                self.totalTimeTakenPerWeek = self.totalTimeTaken / self.studentTasks[0].numberOfWeek / 60;
                self.totalTimeTakenPerWeek = self.totalTimeTakenPerWeek.toFixed(2);
                self.costPerWeek = self.totalCost / self.studentTasks[0].numberOfWeek;
                self.costPerWeek = self.costPerWeek.toFixed(2);
            }, function (error) {
                if (error.status === 404) {
                    self.isResponseEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
            });
        };
    }]
});
