angular.module("enrollStudent").component("enrollStudent", {
    templateUrl: "tcabs/studentEnroll.html",
    controller: ["StudentEnrollment", function (StudentEnrollment) {
        var self = this;
        self.isServerFailed = false;
        self.isStudentEnrollmentBulkUploadSuccessful = false;
        self.isStudentEnrollmentBulkUploadPartialSuccessful = false;
        self.isFileSizeTooLarge = false;
        self.isRequiredFieldsEmpty = false;
        self.studentEnrollmentFile = null;
        self.invalidRecordFileName = "";

        self.enroll = function () {
            self.isServerFailed = false;
            self.isStudentEnrollmentBulkUploadSuccessful = false;
            self.isStudentEnrollmentBulkUploadPartialSuccessful = false;
            self.isFileSizeTooLarge = false;
            self.isRequiredFieldsEmpty = false;
            self.formData = new FormData();
            self.formData.append("file", self.studentEnrollmentFile);
            StudentEnrollment.save({}, self.formData).$promise.then(function (response) {
                self.isStudentEnrollmentBulkUploadSuccessful = true;
            }).catch(function (error) {
                self.invalidRecordFileName = error.data.message;
                if (error.status === 413) {
                    self.isFileSizeTooLarge = true;
                }
                if (error.status === 500) {
                    self.isRequiredFieldsEmpty = true;
                }
                if (error.status === 400) {
                    self.isStudentEnrollmentBulkUploadPartialSuccessful = true;
                }
            });
        };

        self.download = function () {
            StudentEnrollment.get({
                errorFile: self.invalidRecordFileName
            }).$promise.then(function (response) {
                self.url = URL.createObjectURL(new Blob([response.data]));
                self.a = document.createElement('a');
                self.a.href = self.url;
                self.a.download = response.filename;
                self.a.target = '_blank';
                self.a.click();
                $("#studentEnrollmentFile").reset();
            }).catch(function (error) {

            });
            self.isStudentEnrollmentBulkUploadPartialSuccessful = false;
        };
    }]
});
