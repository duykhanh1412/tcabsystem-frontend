myApp.constant("USER_ROLES", {
    all: "*",
    admin: "ADMIN",
    student: "STUDENT",
    supervisor: "SUPERVISOR",
    convener: "CONVENER"
});

myApp.constant("BACKEND_ENDPOINT", "http://130.56.249.229:8080/tcabs");

myApp.config(["$routeProvider", "$locationProvider", "USER_ROLES", function ($routeProvider, $locationProvider, USER_ROLES) {
    $locationProvider.hashPrefix("!");

    $routeProvider.when('/login', {
        template: '<user-login></user-login>',
        access: {
            loginRequired: false,
            authorizedRoles: [USER_ROLES.all]
        }
    }).when('/users', {
        template: '<user-list></user-list>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/index', {
        template: '<index-page></index-page>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.all]
        }
    }).when('/registerEmployee', {
        template: '<register-employee></register-employee>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/manageEmployee', {
        template: '<manage-employee></manage-employee>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/updateEmployee/:employeeId', {
        template: '<update-employee></update-employee>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/registerStudent', {
        template: '<register-student></register-student>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/manageStudent', {
        template: '<manage-student></manage-student>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/updateStudent/:studentId', {
        template: '<update-student></update-student>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/enrollStudent', {
        template: '<enroll-student></enroll-student>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin, USER_ROLES.convener]
        }
    }).when('/registerProject', {
        template: '<register-project></register-project>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/manageProject', {
        template: '<manage-project></manage-project>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/registerTeam', {
        template: '<register-team></register-team>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/manageTeam', {
        template: '<manage-team></manage-team>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/updateTeam/:teamId', {
        template: '<update-team></update-team>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/manageAllocation', {
        template: '<manage-allocation></manage-allocation>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/updateAllocation/:teamId', {
        template: '<update-allocation></update-allocation>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/updateProject/:projectId', {
        template: '<update-project></update-project>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener]
        }
    }).when('/submitTask', {
        template: '<submit-task></submit-task>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.student]
        }
    }).when('/reportStudentTask', {
        template: '<report-student-task></report-student-task>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener, USER_ROLES.supervisor]
        }
    }).when('/reportStudentWeeklyTask', {
        template: '<report-student-weekly-task></report-student-weekly-task>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener, USER_ROLES.supervisor]
        }
    }).when('/reportTeamTasks', {
        template: '<report-team-tasks></report-team-tasks>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.convener, USER_ROLES.supervisor]
        }
    }).when('/registerUnit', {
        template: '<register-unit></register-unit>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/manageUnit', {
        template: '<manage-unit></manage-unit>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/updateUnit', {
        template: '<update-unit></update-unit>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.admin]
        }
    }).when('/user/profile', {
        template: '<user-profile></user-profile>',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.all]
        }
    }).when('/logout', {
        template: '',
        controller: ["AuthenticationService", function (AuthenticationService) {
            AuthenticationService.logout();
        }],
        access: {
            loginRequired: false,
            authorizedRoles: [USER_ROLES.all]
        }
    }).otherwise({
        redirectTo: '/login'
    });
}]);

myApp.config(function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
});

myApp.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

myApp.run(function ($rootScope, AuthenticationService, $location, $http, Session, USER_ROLES, $q, $timeout, $route, $templateCache) {

    $rootScope.$on('$routeChangeStart', function (event, next) {
        if (next.originalPath === '/login' && $rootScope.authenticated) {
            event.preventDefault();
        } else if (next.access && next.access.loginRequired && !$rootScope.authenticated) {
            event.preventDefault();
            $rootScope.$broadcast("event:auth-loginRequired", {});
        } else if (next.access && next.access.loginRequired && !AuthenticationService.isAuthorized(next.access.authorizedRoles)) {
            event.preventDefault();
            $rootScope.$broadcast("event:auth-forbidden", {});
        }
    });

    $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
        $rootScope.$evalAsync(function () {});
    });

    $rootScope.$on("event:auth-loginConfirmed", function (event, data) {
        Session.create(data);
        $rootScope.account = Session;
        $rootScope.authenticated = true;
        $rootScope.display = false;
        var nextLocationPath = ($rootScope.requestedUrl ? $rootScope.requestedUrl : "/index");
        $location.path("/index");
    });

    $rootScope.$on("event:auth-loginRequired", function (event, data) {
        $rootScope.requestedUrl = $location.path();
        Session.invalidate();
        $rootScope.authenticated = false;
        $location.path("/login");
    });

    $rootScope.$on("event:auth-auth-forbidden", function () {

    });

    $rootScope.$on("event:auth-loginCancelled", function () {
        $location.path("/login").replace();
    });

    AuthenticationService.getAccount();
});
