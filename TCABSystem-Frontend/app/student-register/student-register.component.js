angular.module("registerStudent").component("registerStudent", {
    templateUrl: "tcabs/studentRegister.html",
    controller: ["Student", function (Student) {
        var self = this;
        self.isStudentAlreadyExisting = false;
        self.isRequiredFieldsEmpty = false;
        self.isStudentreated = false;
        self.convener = false;
        self.supervisor = false;
        self.admin = false;
        self.student = new Student();
        self.register = function () {
            self.isStudentAlreadyExisting = false;
            self.isRequiredFieldsEmpty = false;
            self.isStudentreated = false;
            self.student.userroles = [];
            self.student.userroles.push({
                id: 4,
                name: 'STUDENT'
            });
            self.student.$save().then(function (response) {
                self.student.dob = new Date(response.dob);
                self.isStudentCreated = true;
            }).catch(function (error, status) {
                if (error.status === 302) {
                    self.isStudentAlreadyExisting = true;
                }
                if (error.status === 500) {
                    self.isRequiredFieldsEmpty = true;
                }
                if (error.status === 400) {
                    self.isRequiredFieldsEmpty = true;
                }
            });
        };
    }]
});
