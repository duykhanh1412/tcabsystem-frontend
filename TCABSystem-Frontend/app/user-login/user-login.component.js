angular.module("userLogin").component("userLogin", {
    templateUrl: "tcabs/login.html",
    controller: ["AuthenticationService", "$rootScope", function (AuthenticationService, $rootScope) {
        //        $route.reload();
        var self = this;
        this.rememberMe = true;
        this.login = function () {
            $rootScope.authenticationError = false;
            AuthenticationService.login(self.username, self.password, self.rememberMe);
        };
    }]
});
