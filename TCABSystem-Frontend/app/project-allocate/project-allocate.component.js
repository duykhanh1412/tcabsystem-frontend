angular.module("allocateProject").component("allocateProject", {
    templateUrl: "tcabs/projectAllocate.html",
    controller: ["Team", "Unit", "Projects", "Supervisor", "Students", function (Team, Unit, Projects, Supervisor, Students) {
        var self = this;
        self.isTeamMissing = false;
        self.isSupervisorIdEmpty = false;
        self.isTeamAlreadyMeetSupervisor = false;
        self.isStudentIdInvalid = [{
            teamCount: false
        }];
        self.isTeamNameInvalid = false;
        self.isTeamNumberLargerThanAllowed = false;
        self.isProjectAllocatedSuccessfully = false;
        self.isServerFailed = false;
        self.isProjectNotChoosed = false;
        self.teams = [{
            id: 0
        }];
        self.isSupervisorNameExisting = false;
        self.units = [];
        self.units = Unit.query();
        self.project;
        self.supervisorName = "";
        self.supervisor = [];
        self.selectedUnit = 0;
        self.teamName = "";
        self.projects = [];
        self.supervisors = [];
        self.students = [{
            id: 0
        }];
        self.getProjects = function () {
            self.isServerFailed = false;
            self.isProjectNotChoosed = false;
            self.projects = Projects.get({
                unitId: self.selectedUnit
            }, function (response) {

            }, function (error) {
                self.isServerFailed = true;
            });
        };

        self.searchSupervisors = function () {
            self.isSupervisorNameExisting = false;
            self.supervisors = Supervisor.get({
                name: self.supervisorName,
                unitId: self.selectedUnit
            }, function (response) {
                self.isSupervisorNameExisting = true;
            }, function (error) {

            });
        };

        self.selectSupervisor = function () {
            self.supervisorName = self.supervisor[0].user.firstname + ' ' + self.supervisor[0].user.lastname;
            self.teams[0].project = self.project;
            self.teams[0].supervisor = self.supervisor[0];
            self.isSupervisorNameExisting = false;
            self.isSupervisorIdEmpty = false;
        };

        self.addStudent = function () {
            self.isProjectNotChoosed = false;
            self.isSupervisorIdEmpty = false;
            self.isTeamNameInvalid = false;
            if (typeof self.project == 'undefined') {
                self.isProjectNotChoosed = true;
                return;
            }
            if (typeof self.supervisor == 'undefined') {
                self.isSupervisorIdEmpty = true;
                return;
            }
            self.students.push({
                id: 0
            });
            self.isStudentIdInvalid.push({
                teamCount: false
            });
        };
        self.removeStudent = function (index) {
            self.isTeamNumberLargerThanAllowed = false;
            self.students.splice(index, 1);
            self.isStudentIdInvalid.splice(index, 1);
        };

        self.allocate = function () {
            self.isProjectNotChoosed = false;
            self.isTeamMissing = false;
            self.isSupervisorIdEmpty = false;
            self.isTeamNameValid = false;
            self.isTeamNumberLargerThanAllowed = false;
            angular.forEach(self.isStudentIdInvalid, function (invalidTeam) {
                invalidTeam.teamCount = false;
            });
            self.isProjectAllocatedSuccessfully = false;
            self.isTeamCountLargerThanAllowed = false;
            self.isServerFailed = false;
            if (self.teams.length === 0) {
                self.isTeamMissing = true;
            }
            if (typeof self.project == 'undefined') {
                self.isProjectNotChoosed = true;
                return;
            }
            if (typeof self.supervisor[0] == 'undefined') {
                self.isSupervisorIdEmpty = true;
                return;
            }
            if (self.students.length > self.project.maximunmembers) {
                self.isTeamNumberLargerThanAllowed = true;
                return;
            }
            if (self.teamName === "") {
                self.isTeamMissing = true;
                return;
            }
            self.teams[0].teamcount = self.students.length;
            self.teams[0].students = self.students;
            self.teams[0].name = self.teamName;
            Team.save({}, self.teams).$promise.then(function (response) {
                self.isProjectAllocatedSuccessfully = true;
            }).catch(function (error) {
                if (error.status === 400) {
                    self.index = 0;
                    console.log(error.data[0].students.length);
                    if(error.data[0].students.length == 0) {
                        self.isTeamNameInvalid = true;
                        console.log(self.isTeamNameInvalid);
                    }
                    if (error.data[0].teamcount > error.data[0].project.maximunmembers) {
                        self.isTeamNumberLargerThanAllowed = true;
                    }
                    angular.forEach(self.teams[0].students, function (student) {
                        angular.forEach(error.data[0].students, function (invalidStudent) {
                            if (student.id == invalidStudent.id) {
                                self.isStudentIdInvalid[self.index].teamCount = true;
                            }
                        });
                        self.index = self.index + 1;
                    });
                }
                if (error.status === 404) {
                    self.isSupervisorIdEmpty = true;
                }
                if (error.status === 500) {
                    self.isServerFailed = true;
                }
                if (error.stats === 406) {
                    self.isTeamAlreadyMeetSupervisor = true;
                }
            });
        };
    }]
});
