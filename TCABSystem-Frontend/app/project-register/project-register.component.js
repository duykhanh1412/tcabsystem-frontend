angular.module("registerProject").component("registerProject", {
    templateUrl: "tcabs/projectRegister.html",
    controller: ["Project", "Units", "Session", function (Project, Units, Session) {
        var self = this;
        self.isProjectCreated = false;
        self.isProjectAlreadyExisting = false;
        self.isRequiredFieldsEmpty = false;
        self.isFileSizeTooLarge = false;
        self.units = Units.query({
            userType: 'convener',
            id: Session.id
        });
        self.project = new Project();
        self.register = function () {
            self.isProjectCreated = false;
            self.isProjectAlreadyExisting = false;
            self.isRequiredFieldsEmpty = false;
            self.isFileSizeTooLarge = false;
            self.formData = new FormData();
            self.formData.append("projectName", self.project.name);
            self.formData.append("unit", self.project.unit.id);
            self.formData.append("startdate", self.project.unit.startdate);
            self.formData.append("enddate", self.project.unit.enddate);
            self.formData.append("maximunmembers", self.project.maximunmembers);
            self.formData.append("description", self.project.description);
            self.formData.append("budget", self.project.budget);
            Project.save({}, self.formData).$promise.then(function (response) {
                self.isProjectCreated = true;
            }).catch(function (error) {
                if (error.status === 302) {
                    self.isProjectAlreadyExisting = true;
                }
                if (error.status === 500 ||  error.status === 400) {
                    self.isRequiredFieldsEmpty = true;
                }
                if (error.status === 413) {
                    self.isFileSizeTooLarge = true;
                }
            });
        };
    }]
});
